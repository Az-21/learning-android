package none.hope_this_works;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }





    public void on_button1_click(View view) {

        EditText gui_text1 = (EditText) findViewById(R.id.gui_text1);

        EditText gui_text2 = (EditText) findViewById(R.id.gui_text2);



        Log.i("Info", "Button 1 was pressed");

        Log.i("Input Value", gui_text1.getText().toString());

        Log.i("Input Value", gui_text2.getText().toString());

        Toast.makeText(this, "Welcome " + gui_text1.getText().toString(), Toast.LENGTH_LONG).show();


    }



}
